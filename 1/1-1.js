import fs from 'fs';

let s = 0;
let p = null;

fs
    .readFileSync('./input.txt', { encoding: 'utf8' })
    .split('\n')
    .map(v => parseInt(v))
    .forEach(v => {
        if (p && v > p) {
            s++;
        }
        p = v;
    });
console.log(s);
