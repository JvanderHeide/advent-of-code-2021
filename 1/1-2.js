import fs from 'fs';

let s = 0;
let p = null;

fs
    .readFileSync('./input.txt', { encoding: 'utf8' })
    .split('\n')
    .map(v => parseInt(v))
    .forEach((v, i, a) => {
        const w = a[i] + a[i + 1] + a[i + 2];
        if (p && w > p) {
            s++;
        }
        p = w;
    });
console.log(s);
