import {readFileSync} from 'fs';

const sumWindow = (i, a, size = 3) => {
    let w = 0;
    for (let j = 0; j < size; j++) {
        w = (i + j) > a.length ? w : w + a[i + j];
    }
    return w;
};

const r = readFileSync('./input.txt', { encoding: 'utf8' })
    .split('\n')
    .map(v => parseInt(v))
    .filter(isFinite)
    .map((v,i,a) => sumWindow(i, a))
    .reduce(({s, p}, v) => ({
        s: s + ((p != null && v > p) ? 1 : 0),
        p: v
    }), {
        s: 0,
        p: null
    });

console.log(r.s);
