import {readFileSync} from 'fs';

const r = readFileSync('./input.txt', { encoding: 'utf8' })
    .split('\n')
    .filter(v => v)
    .reduce((acc, v) => {
        for (let i = 0; i < v.length; i++) {
            if (!Array.isArray(acc[i])) {
                acc[i] = [];
            }
            acc[i].push(parseInt(v[i]));
        }
       return acc;
    },[])
    .map(a => {
        const size0 = a.filter(n => n === 0).length;
        const size1 = a.filter(n => n === 1).length;
        const v = size0 > size1 ? 0 : 1;
        return {
            g: v,
            e: v === 0 ? 1 : 0
        }
    })
    .reduce((acc, v) => {
        acc.g += v.g;
        acc.e += v.e;
        return acc;
    }, {
        g: '', e: ''
    });

console.log(r, parseInt(r.g, 2) * parseInt(r.e, 2));
