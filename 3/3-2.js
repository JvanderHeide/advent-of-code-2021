import {readFileSync} from 'fs';

const input = readFileSync('./input.txt', {encoding: 'utf8'})
    .split('\n')
    .filter(v => v);

const getCountAtPosition = (numbers, position = 0) => {
   return numbers.reduce((acc, number) => {
        const bitAtPosition = parseInt(number[position]);
        acc[bitAtPosition] += 1;
        return acc;
    }, [0, 0]);
}

const findUntilOne = (numbers, criterion, position = 0) => {
    const countAtPosition = getCountAtPosition(numbers, position);
    let filter = numbers.filter(number => {
        const bitAtPosition = parseInt(number[position]);
        const criterionValue = criterion(bitAtPosition, countAtPosition);
        return bitAtPosition === criterionValue;
    });
    if (filter.length > 1) {
        return findUntilOne(filter, criterion,position + 1);
    }
    return filter[0];
}

const mostCommon = (bitAtPosition, countAtPosition) => {
    return countAtPosition[0] > countAtPosition[1] ? 0 : 1;
}
const leastCommon = (bitAtPosition, countAtPosition) => {
    return countAtPosition[0] <= countAtPosition[1] ? 0 : 1;
}

const o2 = findUntilOne(input, mostCommon);
const co2 = findUntilOne(input, leastCommon);

const o2Decimal = parseInt(o2, 2);
const co2Decimal = parseInt(co2, 2);

console.log(`${o2} * ${co2} =\n${o2Decimal} * ${co2Decimal} =\n${o2Decimal * co2Decimal}`);

