import {readFileSync} from 'fs';

class Submarine {
    #position = {x: 0, y: 0, z:0};

    /*
     *            y+
     *           /
     *          /
     * x- ~~~~ 0 ~~~~ x+
     *         |
     *         |
     *         z+
     */

    up(n) {
        this.#position.z -= n;
        if (this.#position.z < 0) {
            this.#position.z = 0;
        }
    }

    down(n) {
        this.#position.z += n;
    }

    forward(n) {
        this.#position.y += n;
    }

    out() {
        return this.#position.y * this.#position.z;
    }
}

const r = readFileSync('./input.txt', { encoding: 'utf8' })
    .split('\n')
    .filter(v => v)
    .map(l => l.split(' '))
    .reduce((sub, [method, value]) => {
        if (typeof sub[method] === 'function') {
            sub[method](parseInt(value));
        }
        return sub;
    }, new Submarine());

console.log(r.out());
