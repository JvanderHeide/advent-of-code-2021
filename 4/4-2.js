import {readFileSync} from 'fs';

const r = readFileSync('./input.txt', {encoding: 'utf8'})
    .split('\n\n')
    .filter(v => v)
    .reduce((acc, v, i) => {
        if (i === 0) {
            acc.numbers = v
                .split(',')
                .map(n => parseInt(n));
        } else {
            const board = v
                .split('\n')
                .filter(l => l)
                .map(l => l
                    .split(' ')
                    .filter(s => s)
                    .map(s => parseInt(s))
                    .map(n => ({n, marked: false}))
                );
            acc.boards.push(board);
        }
        return acc;
    }, {numbers: [], boards: []});

const validateBoard = (board) => {
    let markedInColumn = [0, 0, 0, 0, 0];
    for (let rowIndex = 0; rowIndex < board.length; rowIndex++) {
        const row = board[rowIndex];
        let markedInRow = 0;
        for (let columnIndex = 0; columnIndex < row.length; columnIndex++) {
            const col = row[columnIndex];
            if (col.marked === true) {
                markedInColumn[columnIndex] += 1;
                markedInRow += 1;
            }
        }
        const rowIsBingo = markedInRow === row.length;
        if (rowIsBingo) {
            return true;
        }
    }
    // Check if any of the column is bingo
    return markedInColumn.filter(colCount => colCount === board.length).length > 0;
};

const sumUnmarked = (board) => {
    return board.reduce((acc, row) => {
        const rowSum = row
            .filter(col => !col.marked)
            .reduce((acc, col) => acc + col.n, 0);
        return acc + rowSum;
    }, 0);
};

const winners = [];
let number;
for (let i = 0; i < r.numbers.length; i++) {
    if (winners.length === r.boards.length) {
        const winner = winners[winners.length - 1];
        const sum = sumUnmarked(winner);
        console.log('BINGO! We have a winner!', winner, {sum, number}, sum * number);
        break;
    }
    number = r.numbers[i];
    console.log(`We've drawn: ${number}`);

    for (let boardIndex = 0; boardIndex < r.boards.length; boardIndex++) {
        const board = r.boards[boardIndex];
        if (winners.includes(board)) {
            // Already won, no need to add numbers
            continue;
        }
        for (let rowIndex = 0; rowIndex < board.length; rowIndex++) {
            const row = board[rowIndex];
            for (let columnIndex = 0; columnIndex < row.length; columnIndex++) {
                const column = row[columnIndex];
                if (column.n === number) {
                    column.marked = true;
                }
            }
        }
        if (validateBoard(board)) {
            winners.push(board);
        }
    }
}
